<?php
/**
 * @file
 * Contain lc_telegram moodule delivery class lcTelegramDelivery.
 */
class lcTelegramDelivery extends leadCaptureDelivery {

  /**
   * Getting telegram bot key from saved variable.
   *
   * @return string
   *    bot key
   */
  private static function getBotKey() {
    return variable_get('lc_telegram_bot_key');
  }

  /**
   * Getting telegram chat id from saved variable.
   *
   * @return string
   *    Chat id.
   */
  private static function getChatId() {
    return variable_get('lc_telegram_chat_id');
  }

  /**
   * Run main delivery functionally.
   */
  public static function run($actionHandler) {
    $context = $actionHandler->getContext();
    $data = $actionHandler->getData();

    $url = 'https://api.telegram.org/bot' . self::getBotKey() . '/sendMessage';
    $message = array(
      'text' => $context['title'] . "\n" . implode("\n", $data),
      'chat_id' => self::getChatId(),
    );

    self::sendMessage($url, $message);
  }

  /**
   * Send message helerp callback method.
   */
  public static function sendMessage($url, $message) {
    // Check if proxy enabled, send with proxy auth by curl.
    $has_proxy = variable_get('lc_telegram_enable_proxy', FALSE);
    if($has_proxy){
      $request = self::send_http_query($url, $message);
    }else{
      $options = array(
        'method' => 'POST',
        'data' => drupal_json_encode($message),
        'timeout' => 15,
        'headers' => array('Content-Type' => 'application/json'),
      );
      $request = drupal_http_request($url, $options);
    }

    // Check returned data for errors.
    $request_data = drupal_json_decode($request);
    if(!isset($request_data['ok']) || $request_data['ok'] !== TRUE) {
      $message = 'Error code: !error_code. Description: !description';
      $options = array(
        '!error_code' => $request_data['error_code'],
        '!description' => $request_data['description']
      );
      watchdog('LC Telegram',$message, $options, WATCHDOG_ERROR);
    }
  }

  /**
   * HTTP query send using curl and proxy auth.
   */
  public static function send_http_query($url, $data){
    // Gettting proxy auth data.
    $proxy_login = variable_get('lc_telegram_proxy_login');
    $proxy_pass = variable_get('lc_telegram_proxy_pass');
    $proxy_host = variable_get('lc_telegram_proxy_host');
    $proxy_port = variable_get('lc_telegram_proxy_port');
    // Prepare proxy params for CURl.
    $proxy_url = $proxy_host . ':' . $proxy_port;
    $proxy_auth = $proxy_login . ':' . $proxy_pass;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_PROXY, $proxy_url);
    curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxy_auth);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST,'POST');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $curl_scraped_page = curl_exec($ch);
    curl_close($ch);
    return $curl_scraped_page;
  }

}
